import * as Font from "expo-font";
import React, {useState} from "react";
import AppLoading from 'expo-app-loading';
import SignIn from "./src/screens/auth/SignIn";
import SignUp from "./src/screens/auth/SignUp";
import ForgotPassword from "./src/screens/auth/ForgotPassword";
import Otp from "./src/screens/auth/Otp";
import CreateNewPassword from "./src/screens/auth/CreateNewPassword";

const getFont = () => {
  return Font.loadAsync({
    "avenir-next-reguler": require("./assets/fonts/AvenirNextLTPro-Regular.otf"),
    "avenir-next-demibold": require("./assets/fonts/AvenirNext-DemiBold.ttf"),
    "avenir-next-medium": require("./assets/fonts/AvenirNextCyr-Medium.ttf"),
    "avenir-next-it": require("./assets/fonts/AvenirNextLTPro-It.otf"),
    "avenir-next-bold": require("./assets/fonts/AvenirNextLTPro-Bold.otf"),
  });
};

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  if (fontsLoaded) {
    return (<CreateNewPassword/>);
    // return (<MyStack />)
  } else {
    return (
      <AppLoading startAsync={getFont} onFinish={() => setFontsLoaded(true)} onError={() => console.log('error')}/>
    );
  }
}
