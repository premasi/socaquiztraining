import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import GoogleLogo from "../../../assets/images/brand/Google.svg";

const FormButtonGoogle = ({
  label = "",
  handleOnPress = null,
  style,
  ...more
}) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 13,
        padding: 16,
        backgroundColor: "white",
        borderRadius: 8.39,
        borderWidth: 1,
        borderColor: "white",
        alignSelf: "stretch",
        marginHorizontal: 38,
        ...style,
      }}
      activeOpacity={0.9}
      onPress={handleOnPress}
    >
      <GoogleLogo width={18} height={20} />
      <Text
        style={{
          fontFamily: "avenir-next-demibold",
          color: "black",
          textAlign: "center",
          fontSize: 14,
          marginLeft: 7,
        }}
      >
        {label}
      </Text>
    </TouchableOpacity>
  );
};

export default FormButtonGoogle;

const styles = StyleSheet.create({});
