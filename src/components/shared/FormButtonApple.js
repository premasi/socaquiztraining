import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import AppleLogo from "../../../assets/images/brand/Apple.svg";

const FormButtonApple = ({
  label = "",
  handleOnPress = null,
  style,
  ...more
}) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 23,
        padding: 16,
        backgroundColor: "black",
        borderRadius: 8.39,
        borderWidth: 1,
        borderColor: "white",
        alignSelf: "stretch",
        marginHorizontal: 38,
        ...style,
      }}
      activeOpacity={0.9}
      onPress={handleOnPress}
    >
      <AppleLogo width={18} height={20} />
      <Text
        style={{
          fontFamily: "avenir-next-demibold",
          color: "white",
          textAlign: "center",
          fontSize: 14,
          marginLeft: 7,
        }}
      >
        {label}
      </Text>
    </TouchableOpacity>
  );
};

export default FormButtonApple;

const styles = StyleSheet.create({});
