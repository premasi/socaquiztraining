import { StyleSheet, Text, View, TextInput } from "react-native";
import React from "react";

const FormInput = ({
  label = "",
  placeholder = "",
  onChangeText = null,
  value = null,
  ...more
}) => {
  return (
    <View style={styles.formContainer}>
      <Text style={styles.labelStyle}>{label}</Text>
      <TextInput
        style={styles.formStyle}
        placeholder={placeholder}
        placeholderTextColor="#8C8C8C"
        onChangeText={onChangeText}
        value={value}
        {...more}
      />
    </View>
  );
};

export default FormInput;

const styles = StyleSheet.create({
  formContainer: {
    alignSelf: "stretch",
    marginHorizontal: 38,
    marginTop: 13,
  },
  labelStyle: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 14,
  },
  formStyle: {
    fontFamily: "avenir-next-reguler",
    color: "#fff",
    padding: 10,
    borderColor: "#8C8C8C",
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 8,
  },
});
