import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import React from "react";
import ArrowBackSVG from "../../../assets/images/icon/arrowBack.svg";
import LogoSVG from "../../../assets/images/logo/soca.svg";
import FooterSVG from "../../../assets/images/footer/footer.svg";
import FormInput from "../../components/shared/FormInput";

const ForgotPassword = () => {
  return (
    <SafeAreaView style={styles.container}>
      {/* Header */}
      <TouchableOpacity style={styles.headerStyle}>
        <ArrowBackSVG width={10} height={5} />
        <Text style={styles.headerBackText}>Kembali</Text>
      </TouchableOpacity>

      {/* Body */}
      <View style={styles.bodyContainer}>
        {/* Logo */}
        <LogoSVG width={170} height={41} />

        {/* Screen Title */}
        <Text style={styles.titleForgot}>Lupa Password</Text>

        {/* Email */}
        <FormInput label="Email" placeholder="Masukan email kamu" />

        {/* Login Button / Submit */}
        <TouchableOpacity style={styles.submitButtonStyle}>
          <Text style={styles.submitTextStyle}>Kirim</Text>
        </TouchableOpacity>
      </View>

      {/* Footer */}
      <View style={styles.footerStyle}>
        <FooterSVG width={167} height={14} />
      </View>
    </SafeAreaView>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
  bodyContainer: {
    flex: 3,
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
  },
  headerStyle: {
    flex: 1,
    alignSelf: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    marginLeft: 28,
  },
  headerBackText: {
    fontSize: 20,
    fontFamily: "avenir-next-demibold",
    color: "white",
    marginLeft: 11,
  },
  titleForgot: {
    color: "white",
    fontSize: 24,
    alignItems: "center",
    marginTop: 32,
    fontFamily: "avenir-next-demibold",
    marginBottom: 4,
  },
  submitButtonStyle: {
    justifyContent: "center",
    marginTop: 20,
    padding: 16,
    backgroundColor: "#6D75F6",
    borderRadius: 8.39,
    alignSelf: "stretch",
    marginHorizontal: 38,
  },
  submitTextStyle: {
    fontFamily: "avenir-next-demibold",
    textAlign: "center",
    color: "white",
  },
  footerStyle: {
    flex: 1,
    justifyContent: "flex-end",
  },
});
