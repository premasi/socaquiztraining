import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import React from "react";
import Logo from "../../../assets/images/logo/soca.svg";
import FormButtonApple from "../../components/shared/FormButtonApple";
import FormButtonGoogle from "../../components/shared/FormButtonGoogle";
import FormInput from "../../components/shared/FormInput";
import FooterSVG from "../../../assets/images/footer/footer.svg";

const signIn = () => {
  return (
    <ScrollView style={styles.scrollContainer}>
      <View style={styles.container}>
        {/* Soca Logo */}
        <Logo width={170} height={41} style={{ marginTop: 155 }} />

        {/* Screen Title */}
        <Text style={styles.titleLogin}>Login</Text>

        {/* Button */}
        <FormButtonApple label="Continue With Apple" />
        <FormButtonGoogle label="Continue With Google" />

        {/* Another Option Login */}
        <Text style={styles.optionLoginText}>atau masuk dengan</Text>

        {/* Form Email */}
        <FormInput
          label="Email"
          placeholder="Masukan email kamu"
          keyboardType={"email-address"}
        />

        {/* Form Password */}
        <FormInput
          label="Password"
          placeholder="Masukan password kamu"
          keyboardType="default"
          secureTextEntry={true}
        />

        {/* Forgot Password */}
        <TouchableOpacity style={styles.forgotPasswordText}>
          <Text style={styles.forgotPasswordText}>Lupa password?</Text>
        </TouchableOpacity>

        {/* Login Button / Submit */}
        <TouchableOpacity style={styles.submitButtonStyle}>
          <Text style={styles.submitTextStyle}>Masuk</Text>
        </TouchableOpacity>

        {/* Go to Sign Up */}
        <View style={styles.goToSignUpStyle}>
          <Text style={styles.goToSignUpText}>Belum punya akun?</Text>
          <TouchableOpacity>
            <Text style={styles.goToSignUpSpecialText}>Daftar disini</Text>
          </TouchableOpacity>
        </View>

        {/* Footer */}
        <View>
          <FooterSVG
            width={167}
            height={14}
            style={{ marginTop: 110, marginBottom: 26 }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default signIn;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: "black",
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  titleLogin: {
    color: "white",
    fontSize: 24,
    alignItems: "center",
    marginTop: 32,
    fontFamily: "avenir-next-demibold"
  },
  optionLoginText: {
    fontSize: 12,
    color: "white",
    marginTop: 20,
  },
  forgotPasswordText: {
    fontFamily: 'avenir-next-medium',
    alignSelf: "flex-end",
    color: "#6D75F6",
    marginTop: 12,
    marginRight: 18
  },
  submitButtonStyle: {
    justifyContent: "center",
    marginTop: 23,
    padding: 16,
    backgroundColor: "#6D75F6",
    borderRadius: 8.39,
    alignSelf: "stretch",
    marginHorizontal: 38,
  },
  submitTextStyle: {
    fontFamily: "avenir-next-demibold",
    textAlign: "center",
    color: "white",
  },
  goToSignUpStyle: {
    marginTop: 36,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  goToSignUpText: {
    fontFamily: 'avenir-next-reguler',
    color: "white",
    fontSize: 12,
  },
  goToSignUpSpecialText: {
    fontFamily: "avenir-next-demibold",
    marginLeft: 4,
    color: "#6D75F6",
    fontSize: 12,
  },
});
