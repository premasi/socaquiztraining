import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from "react-native";
import React, { useState, useRef } from "react";
import ArrowBackSVG from "../../../assets/images/icon/arrowBack.svg";
import LogoSVG from "../../../assets/images/logo/soca.svg";
import FooterSVG from "../../../assets/images/footer/footer.svg";

const Otp = () => {
  const pin1Ref = useRef(null);
  const pin2Ref = useRef(null);
  const pin3Ref = useRef(null);
  const pin4Ref = useRef(null);
  const pin5Ref = useRef(null);
  const pin6Ref = useRef(null);

  const [pin1, setPin1] = useState("");
  const [pin2, setPin2] = useState("");
  const [pin3, setPin3] = useState("");
  const [pin4, setPin4] = useState("");
  const [pin5, setPin5] = useState("");
  const [pin6, setPin6] = useState("");

  return (
    <SafeAreaView style={styles.container}>
      {/* Header */}
      <TouchableOpacity style={styles.headerStyle}>
        <ArrowBackSVG width={10} height={5} />
        <Text style={styles.headerBackText}>Kembali</Text>
      </TouchableOpacity>

      {/* Body */}
      <View style={styles.bodyContainer}>
        {/* Logo */}
        <LogoSVG width={170} height={41} />

        {/* Screen Title */}
        <Text style={styles.titleOtp}>Kode Verifikasi</Text>

        {/* Desc */}
        <View style={styles.descStyle}>
          <Text style={styles.descText}>
            Masukan kode verifikasi yang dikirimkan
          </Text>
          <Text style={styles.descText}>ke email kamu</Text>
        </View>

        {/* OTP */}
        <View style={styles.otpContainer}>
          <TextInput
            ref={pin1Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin1) => {
              setPin1(pin1);
              if (pin1 !== "") {
                pin2Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin2Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin2) => {
              setPin2(pin2);
              if (pin2 !== "") {
                pin3Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin3Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin3) => {
              setPin3(pin3);
              if (pin3 !== "") {
                pin4Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin4Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin4) => {
              setPin4(pin4);
              if (pin4 !== "") {
                pin5Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin5Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin5) => {
              setPin5(pin5);
              if (pin5 !== "") {
                pin6Ref.current.focus();
              }
            }}
          />
          <TextInput
            ref={pin6Ref}
            style={styles.formStyle}
            maxLength={1}
            keyboardType={"number-pad"}
            onChange={(pin6) => {
              setPin6(pin6);
            }}
          />
        </View>

        {/* Login Button / Submit */}
        <TouchableOpacity style={styles.submitButtonStyle}>
          <Text style={styles.submitTextStyle}>Kirim</Text>
        </TouchableOpacity>

        {/* Go to Sign Up */}
        <View style={styles.sendCodeStyle}>
          <Text style={styles.sendCodeText}>Belum menerima kode?</Text>
          <TouchableOpacity>
            <Text style={styles.sendCodeSpecialText}>Kirim Ulang</Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* Footer */}
      <View style={styles.footerStyle}>
        <FooterSVG width={167} height={14} />
      </View>
    </SafeAreaView>
  );
};

export default Otp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
  bodyContainer: {
    flex: 3,
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
  },
  headerStyle: {
    flex: 1,
    alignSelf: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    marginLeft: 28,
  },
  headerBackText: {
    fontSize: 20,
    fontFamily: "avenir-next-demibold",
    color: "white",
    marginLeft: 11,
  },
  titleOtp: {
    color: "white",
    fontSize: 24,
    alignItems: "center",
    marginTop: 32,
    fontFamily: "avenir-next-demibold",
    marginBottom: 4,
  },
  descStyle: {
    alignItems: "center",
    justifyContent: "center",
  },
  descText: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 14,
  },
  otpContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    alignSelf: "stretch",
    marginHorizontal: 38,
    marginTop: 77
  },
  formStyle: {
    fontFamily: "avenir-next-reguler",
    alignSelf: "center",
    color: "#fff",
    padding: 10,
    borderColor: "#8C8C8C",
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 8,
  },
  submitButtonStyle: {
    justifyContent: "center",
    marginTop: 20,
    padding: 16,
    backgroundColor: "#6D75F6",
    borderRadius: 8.39,
    alignSelf: "stretch",
    marginHorizontal: 38,
  },
  submitTextStyle: {
    fontFamily: "avenir-next-demibold",
    textAlign: "center",
    color: "white",
  },
  sendCodeStyle: {
    marginTop: 49,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sendCodeText: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 12,
  },
  sendCodeSpecialText: {
    fontFamily: "avenir-next-demibold",
    marginLeft: 4,
    color: "#6D75F6",
    fontSize: 12,
  },

  footerStyle: {
    flex: 1,
    justifyContent: "flex-end",
  },
});
