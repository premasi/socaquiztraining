import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import React from "react";
import Logo from "../../../assets/images/logo/soca.svg";
import FormButtonApple from "../../components/shared/FormButtonApple";
import FormButtonGoogle from "../../components/shared/FormButtonGoogle";
import FormInput from "../../components/shared/FormInput";
import FooterSVG from "../../../assets/images/footer/footer.svg";

const SignUp = () => {
  return (
    <ScrollView style={styles.scrollContainer}>
      <View style={styles.container}>
        {/* Soca Logo */}
        <Logo width={170} height={41} style={{ marginTop: 107 }} />

        {/* Screen Title */}
        <Text style={styles.titleRegis}>Buat Akun</Text>

        {/* Button */}
        <FormButtonApple label="Signup With Apple" />
        <FormButtonGoogle label="Signup With Google" />

        {/* Another Option Login */}
        <Text style={styles.optionLoginText}>atau</Text>

        {/* Form Email */}
        <FormInput
          label="Email"
          placeholder="Masukan email kamu"
          keyboardType={"email-address"}
        />

        {/* Form HP */}
        <FormInput
          label="No.HP"
          placeholder="Masukan no hp kamu"
          keyboardType="number-pad"
        />

        {/* Form Password */}
        <FormInput
          label="Password"
          placeholder="Masukan password kamu"
          keyboardType="default"
          secureTextEntry={true}
        />

        {/* Login Button / Submit */}
        <TouchableOpacity style={styles.submitButtonStyle}>
          <Text style={styles.submitTextStyle}>Daftar</Text>
        </TouchableOpacity>

        {/* Law */}
        <View style={styles.lawStyle}>
          <Text style={styles.lawText}>
            Dengan melakukan daftar berarti anda
          </Text>
          <View style={styles.lawInsideStyle}>
            <Text style={styles.lawText}>menyetujui</Text>
            <TouchableOpacity>
              <Text style={styles.lawSpecialText}>syarat dan ketentuan</Text>
            </TouchableOpacity>
            <Text style={styles.lawText2}>yang berlaku</Text>
          </View>
        </View>

        {/* Go to Sign In */}
        <View style={styles.goToSignInStyle}>
          <Text style={styles.goToSignInText}>Sudah punya akun?</Text>
          <TouchableOpacity>
            <Text style={styles.goToSignInSpecialText}>Masuk disini</Text>
          </TouchableOpacity>
        </View>

        <View>
          <FooterSVG
            width={167}
            height={14}
            style={{ marginTop: 67, marginBottom: 26 }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: "black",
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  titleRegis: {
    color: "white",
    fontSize: 24,
    alignItems: "center",
    marginTop: 32,
    fontFamily: "avenir-next-demibold"
  },
  optionLoginText: {
    fontSize: 12,
    color: "white",
    marginTop: 20,
  },
  submitButtonStyle: {
    justifyContent: "center",
    marginTop: 20,
    padding: 16,
    backgroundColor: "#6D75F6",
    borderRadius: 8.39,
    alignSelf: "stretch",
    marginHorizontal: 38,
  },
  submitTextStyle: {
    fontFamily: "avenir-next-demibold",
    textAlign: "center",
    color: "white",
  },
  lawStyle: {
    marginTop: 19,
    alignItems: "center",
    justifyContent: "center",
  },
  lawInsideStyle: {
    flexDirection: "row",
    alignItems: "center",
  },
  lawText: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 12,
  },
  lawText2: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    marginLeft: 4,
    fontSize: 12,
  },
  lawSpecialText: {
    fontFamily: "avenir-next-demibold",
    marginLeft: 4,
    color: "#6D75F6",
    fontSize: 12,
  },
  goToSignInStyle: {
    marginTop: 56,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  goToSignInText: {
    fontFamily: "avenir-next-reguler",
    color: "white",
    fontSize: 12,
  },
  goToSignInSpecialText: {
    fontFamily: "avenir-next-demibold",
    marginLeft: 4,
    color: "#6D75F6",
    fontSize: 12,
  },
});
